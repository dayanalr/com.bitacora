package com.bitacora.bl;

import java.util.ArrayList;
import java.util.Date;

public class ProyectoComercial extends Proyecto {

    private Cliente cliente;

    public ProyectoComercial(int codigo, String nombre, String descripcion, String fechaInicio, String fechaFin, Cliente cliente, Bitacora bitacora, ArrayList<Tecnologia> tecnologias) {
        super(codigo, nombre, descripcion, fechaInicio, fechaFin, bitacora, tecnologias);
        this.cliente = cliente;
    }

    public ProyectoComercial(int codigo, String nombre, String descripcion, String fechaInicio, String fechaFin, Cliente cliente, Bitacora bitacora) {
        super(codigo, nombre, descripcion, fechaInicio, fechaFin, bitacora);
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ProyectoComercial{" + super.toString() + "cliente=" + cliente + '}';
    }

}
