package com.bitacora.tl;

import java.util.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import com.bitacora.dl.DB_Admin;
import com.bitacora.bl.*;
import java.util.ArrayList;
import java.util.stream.LongStream;

public class Controller {

    private Date date;
    private DB_Admin db;

    private Usuario usuario;
    private Actividad actividad;
    private Tecnologia tecnologia;
    private ClienteJuridico clientJ;
    private Contacto contacto;
    private ClienteFisico clienteF;
    private Bitacora bitacora;
    private ProyectoAcademico proyectoAcademico;
    private ProyectoComercial proyectoComercial;

    private ArrayList<Contacto> contactos_Array;
    private static ArrayList<Tecnologia> tecnolgiesArray;
    private ArrayList<Cliente> clientsArray;
    private static ArrayList<Actividad> allActivitiesArray;
    private ArrayList<Entrada> entradasBitacoraArray;
    private static ArrayList<ProyectoAcademico> proyectosAcademicosArray;
    private static ArrayList<ProyectoComercial> proyectosComercialesArray;
    private static Usuario loggedUser;
    private Entrada entrada;
    private static int selectedProjectIndex;
    private static int selectedClientIndex;

    public Controller() {

        Reader reader = new Reader();
        reader.readDataFromDBFile();
        String[] dataBaseInfo = reader.getDatos();

        String dataBase = dataBaseInfo[0];
        String user = dataBaseInfo[1];
        String password = dataBaseInfo[2];

        db = new DB_Admin(dataBase, user, password);
    }

    public void changePassword() {
        String password;
        String repeatPassword;

        do {

            password = JOptionPane.showInputDialog("Por favor ingrese la nueva contraseña");
            repeatPassword = JOptionPane.showInputDialog("Confirme la nueva contraseña");

        } while (password.equals(repeatPassword) == false);

        db.openDBConnection();
        db.changePassword(password, loggedUser.getCorreo());
        db.closeConnection();
        JOptionPane.showMessageDialog(null, "Contraseña cambiada exitosamente");
    }

    public ArrayList<Tecnologia> getTechnologiesArrayForNewProject(int[] indices) {

        ArrayList<Tecnologia> tecnologias = new ArrayList();

        for (int i = 0; i < indices.length; i++) {
            tecnologias.add(tecnolgiesArray.get(indices[i]));
        }

        return tecnologias;

    }

    public void setLoggedUser(Usuario loggedUser) {
        this.loggedUser = loggedUser;
    }

    public Usuario GetLoggedUser() {
        return this.loggedUser;
    }

    public String GetLoogedUserName() {
        return this.loggedUser.getNombre();
    }

    public void registerUser(String name, String firstSurname, String secondSurname, String ID, String gender, String email, String password) {

        db.openDBConnection();
        int id = Integer.parseInt(ID);
        usuario = new Usuario(id, name, firstSurname, secondSurname, gender, email, password);
        if (db.addUser(usuario) == true) {
            JOptionPane.showMessageDialog(null, "Usuario registrado exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "El usuario con ese correo electronico ya existe");
        }
        db.closeConnection();
    }

    public boolean logIn(String email, String password) {
        db.openDBConnection();
        try {
            if ((db.verifyEmail(email)) == true) {
                if ((db.verifyPassword(password, email) == true)) {
                    this.loggedUser = db.getUserByEmail(email);
                    return true;
                } else {
                    JOptionPane.showMessageDialog(null, "Contraseña incorrecta", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "El usuario no existe");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.closeConnection();

        return false;
    }

    public void logOut() {
        this.loggedUser = null;
    }

    public void setSelectedProjectIndex(int index) {
        this.selectedProjectIndex = index;
    }

    public int getSelectedProjectIndex() {
        return this.selectedProjectIndex;
    }

    public void setSelectedClientIndex(int clientIndex) {
        this.selectedClientIndex = clientIndex;
    }

    public int getSelectedClientIndex() {
        return this.selectedClientIndex;
    }

    public String[] getAllAcademicProjects() {

        String[] listaProyectos;

        db.openDBConnection();
        proyectosAcademicosArray = db.getAllAcademicProjects();
        db.closeConnection();

        listaProyectos = new String[proyectosAcademicosArray.size()];

        for (int i = 0; i < proyectosAcademicosArray.size(); i++) {
            listaProyectos[i] = proyectosAcademicosArray.get(i).toString();
        }
        return listaProyectos;

    }

    public boolean verifyExistingUser() {
        db.openDBConnection();
        if (db.verifyExistingUser() == true) {
            return true;
        }
        return false;
    }

    public void modifyUser(String nombre, String pApellido, String sApellido, String genero, String correo, String contrasena) {
        db.openDBConnection();
        if(db.modifyUser(nombre, pApellido, sApellido, genero, correo, contrasena) == true){
            JOptionPane.showMessageDialog(null, "Informacion actualizada exitosamente");
        }else{
            JOptionPane.showMessageDialog(null, "No se ha podido actualizar la informacion");
        }
    }

    public String[] getAllComercialProjects() {
        String[] listaProyectos;

        db.openDBConnection();
        proyectosComercialesArray = db.getAllComercialProjects();
        db.closeConnection();

        listaProyectos = new String[proyectosComercialesArray.size()];

        for (int i = 0; i < listaProyectos.length; i++) {
            listaProyectos[i] = proyectosComercialesArray.get(i).toString();
        }
        return listaProyectos;
    }

    public String getProjectByID(String id) {

        int id_proyecto = Integer.parseInt(id);
        db.openDBConnection();
        String proyecto = db.getProyectoByID(id_proyecto).toString();
        if (proyecto == null) {
            JOptionPane.showConfirmDialog(null, "No existe un proyecto con ese ID");
        }
        return proyecto;
    }

    public void createNewEntry(String fechaInicio, String fechaFin, int indiceActividad, String projectType) {
        int id_bitacora = 0;
        actividad = allActivitiesArray.get(indiceActividad);
        entrada = new Entrada(actividad, fechaInicio, fechaFin);
        if (projectType.equals("COMERCIAL")) {
            id_bitacora = getBitacoraIDByComercialProject();
        } else if (projectType.equals("ACADEMIC")) {
            id_bitacora = getBitacoraIDByAcademicProject();
        }

        db.openDBConnection();
        if (db.addEntradaBitacora(entrada, id_bitacora) == true) {
            JOptionPane.showMessageDialog(null, "Entrada a la bitacora ingresada exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "No se pudo ingresar la entrada a la bitacora");
        }
        db.closeConnection();
    }

    public void createActivity(String nombre, String descripcion) {

        db.openDBConnection();
        int codigo = db.getNextAvailableActivityCode();
        db.closeConnection();
        actividad = new Actividad(codigo, nombre, descripcion);

        db.openDBConnection();
        if (db.addActivity(actividad) == false) {
            JOptionPane.showMessageDialog(null, "Esta actividad ya existe");
        } else {
            JOptionPane.showMessageDialog(null, "Actividad registrada exitosamente");
        }
        db.closeConnection();
    }

    public void createFClient(String nombre, String pApellido, String sApellido, String ID, String provincia, String canton, String distrito, String direccion, String correo, String telefono) {
        int cedula = Integer.parseInt(ID);
        db.openDBConnection();
        String tipo = "FISICO";
        clienteF = new ClienteFisico(tipo, nombre, pApellido, sApellido, cedula, provincia, canton, distrito, direccion, correo, telefono);
        if (db.addClienteFisico(clienteF, tipo) == true) {
            JOptionPane.showMessageDialog(null, "Cliente registrado exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "Este cliente ya existe");
        }
        db.closeConnection();
    }

    public void createJClient(String nombre, String ID, String provincia, String canton, String distrito, String direccionExacta, String telefono, int indiceContacto) {
        int identificacion = Integer.parseInt(ID);
        db.openDBConnection();
        Contacto contacto = contactos_Array.get(indiceContacto);
        String tipo = "JURIDICO";
        clientJ = new ClienteJuridico(tipo, nombre, identificacion, provincia, canton, distrito, direccionExacta, telefono, contacto);
        db.addClienteJuridico(clientJ, tipo);
        db.closeConnection();

    }

    public void createContact(String nombre, String primerApellido, String segundoApellido, String cedula, String telefono, String correo) {

        db.openDBConnection();
        int idContacto = Integer.parseInt(cedula);
        contacto = new Contacto(idContacto, nombre, primerApellido, segundoApellido, telefono, correo);
        db.addContacto(contacto);
        db.closeConnection();
    }

    public void createTecnology(String nombre, String cod) {

        db.openDBConnection();
        int codigo = Integer.parseInt(cod);
        tecnologia = new Tecnologia(codigo, nombre);
        if (db.addTecnology(tecnologia) == true) {
            JOptionPane.showMessageDialog(null, "Tecnologia registrada exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "Esa tecnologia ya esta registrada");
        }
    }

    public void createAcademicProject(String nombre, String descripcion, String fechaInicio, String fechaFin, int[] tecnologiesIndexes) {
        ArrayList<Tecnologia> tecnologias = new ArrayList();
        for (int i = 0; i < tecnologiesIndexes.length; i++) {
            tecnologias.add(tecnolgiesArray.get(tecnologiesIndexes[i]));
        }

        db.openDBConnection();
        int code = db.getNextAvailableProjectCode();
        proyectoAcademico = new ProyectoAcademico(code, nombre, descripcion, fechaInicio, fechaFin, loggedUser, null, tecnologias);

        if (db.addAcademicProject(proyectoAcademico) == true) {
            JOptionPane.showMessageDialog(null, "Proyecto registrado exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "Este proyecto ya existe");
        }
        db.closeConnection();
    }

    public void createComercialProject(String nombre, String descripcion, String fechaInicio, String fechaFin, int indiceCliente, int[] tecnologiesIndexes) {
        ArrayList<Tecnologia> tecnologias = new ArrayList();
        for (int i = 0; i < tecnologiesIndexes.length; i++) {
            tecnologias.add(tecnolgiesArray.get(tecnologiesIndexes[i]));
        }
        db.openDBConnection();
        int code = db.getNextAvailableProjectCode();
        Cliente selectedClient = clientsArray.get(indiceCliente);
        proyectoComercial = new ProyectoComercial(code, nombre, descripcion, fechaInicio, fechaFin, selectedClient, null, tecnologias);

        if (db.addComercialProject(proyectoComercial) == true) {
            JOptionPane.showMessageDialog(null, "Proyecto registrado exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "Este proyecto ya existe");
        }
        db.closeConnection();
    }

    public void createBitacora(String id_bitacora) {
        db.openDBConnection();
        int idBitacora = Integer.parseInt(id_bitacora);
        ArrayList entradas = db.getAllEntriesByBitacoraID(idBitacora);

        bitacora = new Bitacora(idBitacora, entradas);

        db.closeConnection();
    }

    public String[] getAllEntriesByBitacoraID(int bitacora) {

        String[] entradasBitacora;

        db.openDBConnection();
        entradasBitacoraArray = db.getAllEntriesByBitacoraID(bitacora);
        db.closeConnection();

        entradasBitacora = new String[entradasBitacoraArray.size()];

        if (entradasBitacoraArray.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Aun no hay entradas para esta bitacora"
            );
        }
        for (int i = 0; i < entradasBitacoraArray.size(); i++) {
            entradasBitacora[i] = entradasBitacoraArray.get(i).toString();
        }
        return entradasBitacora;
    }

    public int getBitacoraIDByAcademicProject() {

        return proyectosAcademicosArray.get(this.selectedProjectIndex).getBitacora().getBitacoraID();
    }

    public int getBitacoraIDByComercialProject() {

        return proyectosComercialesArray.get(this.selectedProjectIndex).getBitacora().getBitacoraID();
    }

    public String[] getAllContacts() {

        String[] contactos;

        db.openDBConnection();
        contactos_Array = db.getAllContacts();
        db.closeConnection();

        contactos = new String[contactos_Array.size()];

        for (int i = 0; i < contactos_Array.size(); i++) {
            contactos[i] = contactos_Array.get(i).toString();
        }
        return contactos;
    }

    public String[] getAllTecnologies() {

        String[] tecnologias;

        db.openDBConnection();
        tecnolgiesArray = db.getAllTecnologies();
        db.closeConnection();

        tecnologias = new String[tecnolgiesArray.size()];

        for (int i = 0; i < tecnologias.length; i++) {
            tecnologias[i] = tecnolgiesArray.get(i).toString();
        }
        return tecnologias;
    }

    public void modifyTeconologie(String nombre, int indiceTecnologia) {
        int codigoTecnologia = tecnolgiesArray.get(indiceTecnologia).getCodigo();

        db.openDBConnection();
        if (db.modifyTecnologie(nombre, codigoTecnologia) == true) {
            JOptionPane.showMessageDialog(null, "Se ha actualizado la informacion exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "No se ha podido actualizar la informacion");
        }
    }

    public void modifyComercialProject(int indiceProyecto, String nombre, String descripcion, String fechaInicio, String fechaFin) {

        db.openDBConnection();
        ProyectoComercial pc = proyectosComercialesArray.get(indiceProyecto);
        if (db.modifyComercialProject(pc.getCodigo(), nombre, descripcion, fechaInicio, fechaFin) == true) {
            JOptionPane.showMessageDialog(null, "Se ha actualizado la informacion exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "No se ha podido actualizar la informacion");
        }
        db.closeConnection();
    }

    public void modifyAcademicProject(int indiceProyecto, String nombre, String descripcion, String fechaInicio, String fechaFin) {

        db.openDBConnection();
        ProyectoAcademico pa = proyectosAcademicosArray.get(indiceProyecto);
        if (db.modifyComercialProject(pa.getCodigo(), nombre, descripcion, fechaInicio, fechaFin) == true) {
            JOptionPane.showMessageDialog(null, "Se ha actualizado la informacion exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "No se ha podido actualizar la informacion");
        }
        db.closeConnection();
    }

    public String[] getAllClients() {
        String[] clientes;

        db.openDBConnection();
        clientsArray = db.getAllClients();

        clientes = new String[clientsArray.size()];

        for (int i = 0; i < clientes.length; i++) {
            clientes[i] = clientsArray.get(i).toString();
        }
        db.closeConnection();
        return clientes;
    }

    public String getClientByID(String id) {

        int id_cliente = Integer.parseInt(id);
        db.openDBConnection();
        String cliente = db.getClientByID(id_cliente).toString();
        if (cliente == null) {
            JOptionPane.showMessageDialog(null, "No se ha encontrado un cliente con ese numero de ID");
        }
        db.closeConnection();
        return cliente;
    }

    public Cliente getSelectedClientByIndex(int indiceCliente) {

        Cliente cliente = clientsArray.get(indiceCliente);
        return cliente;

    }

    public String[] getAllActvities() {
        String[] actividades;

        db.openDBConnection();
        allActivitiesArray = db.getAllActivities();

        actividades = new String[allActivitiesArray.size()];

        for (int i = 0; i < actividades.length; i++) {
            actividades[i] = allActivitiesArray.get(i).toString();
        }
        db.closeConnection();
        return actividades;
    }

    public void modifyClientInfo(int idCliente, String nombre, String provincia, String canton, String distrito, String direccion, String telefono) {
        db.openDBConnection();
        if (db.modifyClientInfo(idCliente, nombre, provincia, canton, distrito, direccion, telefono) == true) {
            JOptionPane.showMessageDialog(null, "Se han guardado los cambios exitosamente");
        } else {
            JOptionPane.showMessageDialog(null, "No se han podido guardar los cambios");
        }
        db.closeConnection();

    }

    public String getTecnologieByName(String nombre) {

        String tecnologia;

        db.openDBConnection();
        if (db.getTecnologieByName(nombre) == null) {
            JOptionPane.showMessageDialog(null, "No se ha encontrado una tecnologia con ese nombre");
        }
        tecnologia = db.getTecnologieByName(nombre).toString();
        db.closeConnection();
        return tecnologia;
    }

}
