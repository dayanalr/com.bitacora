package com.bitacora.dl;

import java.util.logging.Level;
import java.util.logging.Logger;
import com.bitacora.bl.*;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DB_Admin {

    private Connection con;
    private PreparedStatement pstm;
    private ResultSet rs;
    private Statement st;

    private String db;
    private String user;
    private String password;

    //constructor
    public DB_Admin(String db, String user, String password) {
        this.db = db;
        this.user = user;
        this.password = password;
    }

    public Connection openDBConnection() {

        try {
            con = DriverManager.getConnection(db, user, password);
        } catch (Exception e) {
            System.out.println(e);
        }
        return con;
    }

    //VERIFY METHODS
    public boolean verifyEmail(String email) throws SQLException {

        String sql = "SELECT correo_electronico FROM usuario WHERE correo_electronico = '" + email + "' ";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            if (rs != null && rs.next()) {
                if (rs.getString("correo_electronico").equals(email)) {
                    return true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    public boolean verifyExistingUser() {
        String sql = "SELECT * FROM usuario";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            if (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Usuario getUserByEmail(String email) {
        String sql = "SELECT id, nombre, primer_apellido, segundo_apellido, genero, correo_electronico, contraseña FROM usuario WHERE correo_electronico = '" + email + "' ";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            if (rs != null && rs.next()) {

                return new Usuario(rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("primer_apellido"),
                        rs.getString("segundo_apellido"),
                        rs.getString("genero"),
                        rs.getString("correo_electronico"),
                        null
                );

            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    public boolean verifyPassword(String password, String email) {

        String sql = "SELECT * FROM usuario WHERE  correo_electronico = '" + email + "' ";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            //System.out.println(rs);

            if (rs.next() && (rs.getString("contraseña")).equals(password)) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean verifyCliente(Cliente obj) {
        String firstSql = "SELECT * FROM cliente WHERE id_cliente = '" + obj.getIdentificacion() + "' ";

        try {
            pstm = con.prepareStatement(firstSql);
            rs = pstm.executeQuery();

            if (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean changePassword(String newPass, String email) {
        String sql = "UPDATE usuario SET contraseña = '" + newPass + "' WHERE correo_electronico = '" + email + "' ";
        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    public boolean modifyClientInfo(int id, String nombre, String provincia, String canton, String distrito, String direccion, String telefono) {

        String sql = "UPDATE cliente SET nombre = '" + nombre + "' , provincia = '" + provincia + "', canton = '" + canton + "', distrito = '" + distrito + "', "
                + "direccion_exacta = '" + direccion + "', telefono = '" + telefono + "' WHERE id_cliente = '" + id + "' ";
        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();

            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean modifyComercialProject(int idProyecto, String nombre, String descripcion, String fechaInicio, String fechaFin) {
        String sql = "UPDATE proyecto SET nombre = '" + nombre + "' , descripcion = '" + descripcion + "', fecha_inicio = '" + fechaInicio + "', fecha_fin = '" + fechaFin + "', "
                + "' WHERE id = '" + idProyecto + "' ";

        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();

            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean modifyAcademicProject(int idProyecto, String nombre, String descripcion, String fechaInicio, String fechaFin) {
        String sql = "UPDATE proyecto SET nombre = '" + nombre + "' , descripcion = '" + descripcion + "', fecha_inicio = '" + fechaInicio + "', fecha_fin = '" + fechaFin + "', "
                + "' WHERE id = '" + idProyecto + "' ";
        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();

            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean modifyUser(String nombre, String pApellido, String sApellido, String genero, String correo, String contrasena) {
        String sql = "UPDATE usuario SET nombre = '" + nombre + "', primer_apellido = '" + pApellido + "', segundo_apellido = '" + sApellido + "', genero = '" + genero + "', correo_electronico = '" + correo + "', contraseña = '" + contrasena + "' ";

        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();
            
            return true;
            
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    //ADD METHODS
    public boolean addUser(Usuario user) {

        try {
            boolean existingEmail = verifyEmail(user.getCorreo());
            if (existingEmail == false) {

                String sql = "INSERT INTO usuario VALUE('" + user.getIdentificacion() + "','" + user.getNombre() + "','" + user.getPrimerApellido()
                        + "','" + user.getSegundoApellido() + "','" + user.getGenero() + "','" + user.getCorreo() + "','" + user.getClave() + "');";
                pstm = con.prepareStatement(sql);
                pstm.execute();
                return true;
            }
        } catch (SQLException ex) {

            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean addActivity(Actividad obj) {

        String firstSql = "SELECT * FROM actividad WHERE id_actividad = '" + obj.getCodigo() + "' ";
        String secondSql = "INSERT INTO actividad VALUE( '" + obj.getCodigo() + "','" + obj.getNombre() + "','" + obj.getDescripcion() + "')";

        try {
            System.out.println(secondSql);
            pstm = con.prepareStatement(firstSql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                return false;
            } else {
                pstm = con.prepareStatement(secondSql);
                pstm.execute();
            }

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public boolean addClienteFisico(ClienteFisico obj, String tipo) {

        String sql = "INSERT INTO cliente VALUE('" + tipo + "','" + obj.getIdentificacion() + "','" + obj.getNombre() + "', null , null ,'" + obj.getProvincia() + "','"
                + obj.getCanton() + "','" + obj.getDistrito() + "','" + obj.getDireccionExacta() + "','" + obj.getTelefono() + "','" + obj.getCorreo() + "', null )";

        try {
            if (verifyCliente(obj) == false) {

                System.out.println(sql);

                pstm = con.prepareStatement(sql);
                pstm.execute();

                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean addClienteJuridico(ClienteJuridico cj, String tipo) {

        String sql = "INSERT INTO cliente VALUE('" + tipo + "','" + cj.getIdentificacion() + "','" + cj.getNombre() + "','" + null + "','" + null + "','" + cj.getProvincia() + "','" + cj.getCanton() + "','"
                + cj.getDistrito() + "','" + cj.getDireccionExacta() + "','" + cj.getTelefono() + "','" + null + "','" + cj.getContacto().getIdentificacion() + "')";
        try {
            if (verifyCliente(cj) == false) {
                pstm = con.prepareStatement(sql);
                pstm.execute();

                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean addContacto(Contacto obj) {

        String sql = "INSERT INTO contacto VALUE('" + obj.getIdentificacion() + "','" + obj.getNombre() + "','" + obj.getPrimerApellido() + "','" + obj.getSegundoApellido() + "','" + obj.getTelefono() + "','" + obj.getCorreo() + "')";

        System.out.println(sql);
        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean add_Client_Contact_ID(int ID_cliente, int ID_contacto) {

        String firstSql = "SELECT * FROM cliente_juridico WHERE id_juridico = '" + ID_cliente + "' AND id_contacto = '" + ID_contacto + "' ";
        String secSql = "INSERT INTO cliente_juridico VALUE('" + ID_cliente + "','" + ID_contacto + "')";

        try {
            pstm = con.prepareStatement(firstSql);
            rs = pstm.executeQuery();

            if (rs.next()) {
                return false;
            } else {
                pstm = con.prepareStatement(secSql);
                pstm.execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public boolean addAcademicProject(ProyectoAcademico obj) {

        String sql = "INSERT INTO bitacora.proyecto (tipo_proyecto, id, nombre, descripcion, fecha_inicio, fecha_fin, cedula_usuario) "
                + "VALUES ( 'ACADEMICO' , '" + obj.getCodigo() + "','" + obj.getNombre() + "','" + obj.getDescripcion() + "','" + obj.getFechaInicio() + "','" + obj.getFechaFin() + "','" + obj.getUsuario().getIdentificacion() + "')";

        try {

            pstm = con.prepareStatement(sql);
            pstm.execute();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean addComercialProject(ProyectoComercial obj) {
        String sql = "INSERT INTO bitacora.proyecto (tipo_proyecto, id, nombre, descripcion, fecha_inicio, fecha_fin, id_cliente) "
                + "VALUES ( 'COMERCIAL' , '" + obj.getCodigo() + "','" + obj.getNombre() + "','" + obj.getDescripcion() + "','" + obj.getFechaInicio() + "','" + obj.getFechaFin() + "','" + obj.getCliente().getIdentificacion() + "')";

        System.out.println(sql);
        try {

            pstm = con.prepareStatement(sql);
            pstm.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int getNextAvailableProjectCode() {

        String sql = "select max(id) id from bitacora.proyecto";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            if (rs.next()) {
                return rs.getInt("id") + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);

        }
        return 1;

    }

    public int getNextAvailableActivityCode() {

        String sql = "select max(id_actividad) id from bitacora.actividad";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            if (rs.next()) {
                return rs.getInt("id") + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);

        }
        return 1;

    }

    public boolean addEntradaBitacora(Entrada obj, int id_bitacora) {

        String sql = "INSERT INTO bitacora_actividades (id_bitacora, id_actividad, fecha_inicio, fecha_fin) "
                + "  VALUE('" + id_bitacora + "','" + obj.getActividad().getCodigo() + "','" + obj.getFechaInicio() + "','" + obj.getFechaFin() + "') ";

        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    //GET METHODS 
    public ArrayList<Contacto> getAllContacts() {

        ArrayList<Contacto> contactos = new ArrayList();
        String sql = "select id_contacto, nombre, primer_apellido , segundo_apellido, telefono, correo from bitacora.contacto";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            while (rs.next()) {
                contactos.add(new Contacto(rs.getInt("id_contacto"), rs.getString("nombre"), rs.getString("primer_apellido"), rs.getString("segundo_apellido"),
                        rs.getString("telefono"), rs.getString("correo")));
            }
            return contactos;
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    public ArrayList<ProyectoAcademico> getAllAcademicProjects() {

        ResultSet rs;
        ArrayList<ProyectoAcademico> proyectosAcademicos = new ArrayList();
        String sql = "select id, nombre, descripcion, fecha_inicio, fecha_fin, cedula_usuario, id_bitacora from bitacora.proyecto where tipo_proyecto = 'ACADEMICO' ";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            while (rs.next()) {
                Usuario usuario = getUserByID(rs.getInt("cedula_usuario"));
                Bitacora bitacora = getBitacoraByID(rs.getInt("id_bitacora"));

                proyectosAcademicos.add(new ProyectoAcademico(rs.getInt("id"), rs.getString("nombre"), rs.getString("descripcion"),
                        rs.getString("fecha_inicio"), rs.getString("fecha_Fin"),
                        usuario, bitacora));
            }
            System.out.println(proyectosAcademicos);

            return proyectosAcademicos;
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<ProyectoComercial> getAllComercialProjects() {

        ResultSet rs;
        ArrayList<ProyectoComercial> proyectosComerciales = new ArrayList();
        String sql = "select tipo_proyecto, id, nombre, descripcion, fecha_inicio, fecha_fin, id_cliente, id_bitacora from bitacora.proyecto where tipo_proyecto = 'COMERCIAL'";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            while (rs.next()) {
                Cliente cliente = getClientByID(rs.getInt("id_cliente"));
                Bitacora bitacora = getBitacoraByID(rs.getInt("id_bitacora"));

                proyectosComerciales.add(new ProyectoComercial(rs.getInt("id"), rs.getString("nombre"), rs.getString("descripcion"),
                        rs.getString("fecha_inicio"), rs.getString("fecha_Fin"), cliente, bitacora));
            }
            System.out.println(proyectosComerciales);

            return proyectosComerciales;
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Usuario getUserByID(int id) {
        String sql = "select id, nombre, primer_apellido, segundo_Apellido, genero, correo_electronico, contraseña from bitacora.usuario where id = '" + id + "'";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new Usuario(rs.getInt("id"), rs.getString("nombre"), rs.getString("primer_apellido"),
                        rs.getString("segundo_Apellido"), rs.getString("genero"), rs.getString("correo_electronico"), rs.getString("contraseña"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public Cliente getClientByID(int ID) {
        String sql = "SELECT tipo_cliente, id_cliente, nombre, primer_apellido, segundo_apellido, provincia, canton, distrito, direccion_exacta, telefono, correo, id_contacto FROM cliente WHERE id_cliente = '" + ID + "' ";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                if ((rs.getString("tipo_cliente").equals("FISICO"))) {
                    return new ClienteFisico(null, rs.getString("nombre"), rs.getString("primer_apellido"), rs.getString("segundo_apellido"), rs.getInt("id_cliente"), rs.getString("provincia"), rs.getString("canton"), rs.getString("distrito"), rs.getString("direccion_exacta"), rs.getString("telefono"), rs.getString("correo"));
                } else {
                    return new ClienteJuridico(null, rs.getString("nombre"), rs.getInt("id_cliente"), rs.getString("provincia"), rs.getString("canton"), rs.getString("distrito"), rs.getString("direccion_exacta"), rs.getString("telefono"), getContactoByID(rs.getInt("id_contacto")));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Tecnologia getTecnologieByName(String tecNombre) {

        String sql = "SELECT * FROM tecnologias WHERE nombre = '" + tecNombre + "' ";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new Tecnologia(rs.getInt("id_tecnologia"), rs.getString("nombre"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Contacto getContactoByID(int id) {
        String sql = "SELECT id_contacto, nombre, primer_apellido, segundo_apellido, telefono, correo FROM contacto WHERE id_contacto = '" + id + "' ";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                return new Contacto(rs.getInt("id_contacto"), rs.getString("nombre"), rs.getString("primer_apellido"), rs.getString("segundo_apellido"), rs.getString("telefono"), rs.getString("correo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Bitacora getBitacoraByID(int ID) {
        ResultSet rs;

        String sql = "SELECT id_bitacora, id_actividad, fecha_inicio, fecha_fin FROM bitacora.bitacora_actividades where id_bitacora = '" + ID + "'";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                ArrayList<Entrada> entradas = getAllEntriesByBitacoraID(ID);
                return new Bitacora(ID, entradas);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Bitacora(ID, null);
    }

    public Proyecto getProyectoByID(int ID) {
        ResultSet rs;

        String sql = "SELECT * FROM proyecto WHERE id = '" + ID + "' ";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                Bitacora bitacora = getBitacoraByID(rs.getInt("id_bitacora"));
                return new Proyecto(rs.getInt("id"), rs.getString("nombre"), rs.getString("descripcion"), rs.getString("fecha_inicio"), rs.getString("fecha_fin"), bitacora);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean addTecnology(Tecnologia obj) {
        String firstSql = "SELECT * FROM tecnologias WHERE id_tecnologia = '" + obj.getCodigo() + "' ";
        String secSql = "INSERT INTO tecnologias VALUE('" + obj.getCodigo() + "','" + obj.getNombre() + "')";

        try {
            pstm = con.prepareStatement(firstSql);
            rs = pstm.executeQuery();

            if (rs.next()) {
                return false;
            } else {
                pstm = con.prepareStatement(secSql);
                pstm.execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public ArrayList<Tecnologia> getAllTecnologies() {

        ArrayList<Tecnologia> tecnologias = new ArrayList();
        String sql = "SELECT * FROM tecnologias";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            while (rs.next()) {
                tecnologias.add(new Tecnologia(rs.getInt("id_tecnologia"), rs.getString("nombre")));
            }
            return tecnologias;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean modifyTecnologie(String nombre, int codigo) {
        String sql = "UPDATE tecnologias SET nombre = '" + nombre + "' WHERE id_tecnologia = '" + codigo + "' ";

        try {
            pstm = con.prepareStatement(sql);
            pstm.execute();

            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public ArrayList<Cliente> getAllClients() {

        ArrayList<Cliente> clientes = new ArrayList();
        String sql = "SELECT tipo_cliente, id_cliente , nombre, provincia, canton, distrito, direccion_exacta, telefono FROM cliente";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            while (rs.next()) {
                clientes.add(new Cliente(rs.getString("tipo_cliente"), rs.getInt("id_cliente"), rs.getString("nombre"), rs.getString("provincia"),
                        rs.getString("canton"), rs.getString("distrito"), rs.getString("direccion_exacta"), rs.getString("telefono")));
            }
            return clientes;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Actividad> getAllActivities() {

        ArrayList<Actividad> actividades = new ArrayList();
        String sql = "SELECT id_actividad, nombre, descripcion FROM actividad";

        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            while (rs.next()) {
                actividades.add(new Actividad(rs.getInt("id_actividad"), rs.getString("nombre"), rs.getString("descripcion")));
            }
            return actividades;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Entrada> getAllEntriesByBitacoraID(int bitacora) {

        ArrayList allEntries = new ArrayList();
        String sql = "SELECT id_bitacora, BA.id_Actividad as id_actividad, fecha_inicio, fecha_fin, NOMBRE, DESCRIPCION "
                + "from bitacora.bitacora_actividades BA "
                + "INNER JOIN BITACORA.ACTIVIDAD A ON BA.id_Actividad = A.ID_ACTIVIDAD WHERE id_bitacora = '" + bitacora + "' ";
        try {
            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();

            while (rs.next()) {
                Actividad actividad = new Actividad(rs.getInt("id_actividad"), rs.getString("nombre"), rs.getString("descripcion"));
                allEntries.add(new Entrada(actividad, rs.getString("fecha_inicio"), rs.getString("fecha_fin")));
            }
            return allEntries;

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public void closeConnection() {
        try {
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(DB_Admin.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
