package com.bitacora.bl;

import java.util.ArrayList;
import java.util.Date;

public class ProyectoAcademico extends Proyecto {

    private Usuario usuario;

    public ProyectoAcademico(int codigo, String nombre, String descripcion, String fechaInicio, String fechaFin, Usuario usuario, Bitacora bitacora, ArrayList<Tecnologia> tecnologias) {
        super(codigo, nombre, descripcion, fechaInicio, fechaFin, bitacora, tecnologias);
        this.usuario = usuario;
    }

    public ProyectoAcademico(int codigo, String nombre, String descripcion, String fechaInicio, String fechaFin, Usuario usuario, Bitacora bitacora) {
        super(codigo, nombre, descripcion, fechaInicio, fechaFin, bitacora);
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int getCodigo() {
        return codigo;
    }

    @Override
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ProyectoAcademico{" + super.toString() + "usuario=" + usuario + '}';
    }

}
