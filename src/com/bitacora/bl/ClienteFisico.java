package com.bitacora.bl;

public class ClienteFisico extends Cliente{
    
    String primerApellido;
    String segundoApellido;
    String correo;

    public ClienteFisico(String tipo_cliente, String nombre, String primerApellido, String segundoApellido, int identificacion, String provincia, String canton, String distrito, String direccionExacta, String correo, String telefono) {
        super(tipo_cliente, identificacion, nombre,  provincia, canton, distrito, direccionExacta, telefono);
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.correo = correo;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "ClienteFisico: " + "\n" + "primerApellido=" + primerApellido + ", segundoApellido=" + segundoApellido + super.toString() + ", correo=" + correo + '}';
    }
    
    
}
