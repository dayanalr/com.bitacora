package com.bitacora.bl;

import java.util.ArrayList;

public class Bitacora {

    int bitacoraID;
    ArrayList<Entrada> entradas;

    public Bitacora(int bitacoraID, ArrayList<Entrada> entradas) {
        this.bitacoraID = bitacoraID;
        this.entradas = entradas;
    }

    public int getBitacoraID() {
        return bitacoraID;
    }

    public void setBitacoraID(int bitacoraID) {
        this.bitacoraID = bitacoraID;
    }

    public ArrayList<Entrada> getEntradas() {
        return entradas;
    }

    public void setEntradas(ArrayList<Entrada> entradas) {
        this.entradas = entradas;
    }

    @Override
    public String toString() {
        return "Bitacora{" + "bitacoraID=" + bitacoraID + ", entradas=" + entradas + '}';
    }

    
}
