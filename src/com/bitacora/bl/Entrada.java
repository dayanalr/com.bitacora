package com.bitacora.bl;

public class Entrada {

    Actividad actividad;
    String fechaInicio;
    String fechaFin;

    public Entrada(Actividad actividad, String fechaInicio, String fechaFin) {
        this.actividad = actividad;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Override
    public String toString() {
        return "Entrada{" + "actividad=" + actividad + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + '}';
    }

}
