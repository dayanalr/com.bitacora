package com.bitacora.bl;

public class Usuario{

    private String nombre;
    private int identificacion;
    private String primerApellido;
    private String segundoApellido;   
    private String genero;
    private String correo;
    private String clave;

    public Usuario(int identificacion, String nombre, String primerApellido, String segundoApellido, String genero, String correo, String clave) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.genero = genero;
        this.correo = correo;
        this.clave = clave;
    }


    
    //SETTERS
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public void setIdentificacion(int identificacion){
        this.identificacion = identificacion;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public void setClave(String clave){
        this.clave = clave;
    }

    //GETTERS

    public String getNombre(){
        return nombre;
    }
    
    public int getIdentificacion(){
        return identificacion;
    }
    
    public String getPrimerApellido() {
        return primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public String getCorreo() {
        return correo;
    }

    public String getGenero() {
        return genero;
    }
    
    public String getClave(){
        return clave;
    }
    
    @Override
    public String toString() {
        return "Usuario{" + "Nombre=" + nombre + "Cedula=" + identificacion + ", Apellidos=" + primerApellido + " " + segundoApellido +  ", Correo=" + correo + ", Genero=" + genero + "Clave= " + clave + '}';
    }
}

