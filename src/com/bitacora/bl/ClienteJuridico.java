package com.bitacora.bl;

public class ClienteJuridico extends Cliente {

    private Contacto contacto;

    public ClienteJuridico(String tipo_cliente, String nombre, int identificacion, String provincia, String canton, String distrito, String direccionExacta, String telefono, Contacto contacto) {
        super(tipo_cliente, identificacion, nombre, provincia, canton, distrito, direccionExacta, telefono);
        this.contacto = contacto;

    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    public Contacto getContacto() {
        return this.contacto;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDireccionExacta() {
        return direccionExacta;
    }

    public void setDireccionExacta(String direccionExacta) {
        this.direccionExacta = direccionExacta;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
