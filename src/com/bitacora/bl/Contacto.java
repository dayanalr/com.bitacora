
package com.bitacora.bl;

public class Contacto{
    
    private String nombre;
    private int identificacion;
    private String primerApellido;
    private String segundoApellido;
    private String telefono;
    private String correo;
    
    //CONSTRUCTOR

    public Contacto(int identificacion, String nombre, String primerApellido, String segundoApellido, String telefono, String correo) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.telefono = telefono;
        this.correo = correo;
    }

    
    //SETTER
    
    public void setIdentificacion(int identificacion){
        this.identificacion = identificacion;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    //GETTER
    
    public int getIdentificacion(){
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCorreo() {
        return correo;
    }
    
    public String toString(){
        String contacto= "Identificacion: " + identificacion + "Nombre: " + nombre + "\t" + "Apellidos: " + primerApellido + " " + segundoApellido + "\t"  + "Telefono :" + telefono + "\t" + "Correo: " + correo; 
        return contacto;
    }
    
    
    
    
}
