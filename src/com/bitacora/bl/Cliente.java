package com.bitacora.bl;

public class Cliente {
    
    protected String tipo_cliente;
    protected String nombre;
    protected int identificacion;
    protected String provincia;
    protected String canton;
    protected String distrito;
    protected String direccionExacta;
    protected String telefono;

    public Cliente(String tipo_cliente, int identificacion, String nombre, String provincia, String canton, String distrito, String direccionExacta, String telefono) {
        this.tipo_cliente = tipo_cliente;
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.direccionExacta = direccionExacta;
        this.telefono = telefono;
    }

    //SETTERS
    public void setTipoCliente(){
        this.tipo_cliente = tipo_cliente;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setIdentificacion(int identificacion){
        this.identificacion = identificacion;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public void setDireccionExacta(String direccionExacta) {
        this.direccionExacta = direccionExacta;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    //GETTERS
    public String getTipo_Cliente(){
        return this.tipo_cliente;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public int getIdentificacion(){
        return identificacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getCanton() {
        return canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public String getDireccionExacta() {
        return direccionExacta;
    }

    public String getTelefono() {
        return telefono;
    }

    @Override
        public String toString() {
        return "Cliente{" + ", nombre:" + nombre + ", Identificacion: " + identificacion + ", provincia=" + provincia + ", canton=" + canton + ", distrito=" + distrito + ", direccionExacta=" + direccionExacta + ", telefono=" + telefono + '}';
    }
    
    
}
