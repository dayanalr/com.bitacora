package com.bitacora.tl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Reader {

    private String[] datos;

    public String[] readDataFromDBFile() {

        BufferedReader br = null;
        datos = new String[3];

        try {
            br = new BufferedReader(new FileReader("DataBaseAccesInfo.txt"));
            String line = "";

            for (int i = 0; i < datos.length; i++) {

                if ((line = br.readLine()) != null) {
                    datos[i] = line.trim();
                    System.out.println(datos[i]);
                }

            }
            return datos;
        } catch (IOException e) {
            System.out.println(e);
        }
        return null;
    }

    public String[] getDatos() {
      
        return datos;

    }
}
