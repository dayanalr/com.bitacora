package com.bitacora.bl;

import java.util.ArrayList;
import java.util.Date;

public class Proyecto {

    protected int codigo;
    protected String nombre;
    protected String descripcion;
    protected String fechaInicio;
    protected String fechaFin;
    private Bitacora bitacora;
    private ArrayList<Tecnologia> tecnologias;

    public Proyecto(int codigo, String nombre, String descripcion, String fechaInicio, String fechaFin, Bitacora bitacora) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.bitacora = bitacora;
     
    }

    public Proyecto(int codigo, String nombre, String descripcion, String fechaInicio, String fechaFin, Bitacora bitacora, ArrayList<Tecnologia> tecnologias) {

        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.bitacora = bitacora;
    }

    public Bitacora getBitacora() {
        return bitacora;
    }

    public void setBitacora(Bitacora bitacora) {
        this.bitacora = bitacora;
    }

    public ArrayList<Tecnologia> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(ArrayList<Tecnologia> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    @Override
    public String toString() {
        return "Proyecto{" + "codigo=" + codigo + ", nombre=" + nombre + ", descripcion=" + descripcion
                + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + "Bitacora= " + bitacora.getBitacoraID() + '}';
    }

}
